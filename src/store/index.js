import { writable, derived, get } from "svelte/store";
export const apiUrl = "https://dreamapp.screenlife.com";

export const user = writable({});
export const isAuthorized = derived(user, ($user) => $user.username);
export const isAuthOpen = writable(false);
export const refererSlug = writable();

export const getUserData = () => {
  return fetch(`${apiUrl}/api/v1/userprofile/profile/`, {
    credentials: "include",
    headers: { "Content-type": "application/json; charset=utf-8" },
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data)
      if(data.detail==="У вас нет прав для выполнения этой операции.") {
        return false;
      } else {
        user.set(data);
        return true
      }
      
    })
    .catch((err) => {
      console.log(err);
    });
};
getUserData();

export const getVerificationCode = (phone) => {
  return fetch(`${apiUrl}/api/v1/userprofile/register/ `, {
    headers: { "Content-type": "application/json; charset=utf-8" },
    method: "post",
    body: JSON.stringify({ phone, referral: get(refererSlug)  }),
  })
    .then((res) => res.json())
    .then((data) => {
      return data;
    });
};

export const checkVerificationCode = ({ phone, code }) => {
  return fetch(`${apiUrl}/api/v1/userprofile/auth/ `, {
    headers: { "Content-type": "application/json; charset=utf-8" },
    credentials: "include",
    method: "post",
    body: JSON.stringify({ phone, code }),
  })
    .then((res) => res.json())
    .then((data) => {
      if(data.status===401) return false;
      return getUserData();
    });
};

export const submitRequest = (e) => {
  const formData = new FormData(e.target);
  return fetch(`${apiUrl}/api/v1/proposals/save/ `, {
    method: "post",
    body: formData,
  })
    .then((res) => res.json())
    .then((data) => {
      if (!data.slug) throw new Error(data);
    });
};
