function rnd(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}
export const getYoutubeLink = (url) => {
  let iframeUrl = url;
  if (!/^http/.test(url)) iframeUrl = "https://" + url;
  iframeUrl = url.replace("watch?v=", "");
  if (!/^\/\/.+\/embed\//.test(iframeUrl)) {
    iframeUrl = iframeUrl
      .replace(iframeUrl.match(/\/\/.+\//)[0], "//www.youtube.com/embed/")
      .replace("&", "?&");
  }
  return iframeUrl;
};

export function type(node, mistake = node.textContent) {
  const text = node.textContent;
  const { height } = node.getBoundingClientRect();
  node.style.height = height + "px";
  let i = 1;
  let timer;
  let typing = true;
  let typingMistake = true;
  const tick = () => {
    if (!typing && typingMistake && mistake.slice(0, i) === text.slice(0, i)) {
      typingMistake = false;
      typing = true;
    }
    const currentText = typingMistake ? mistake : text;
    node.textContent = currentText.slice(0, i);
    i = i + (typing ? +1 : -1);
    if (i === currentText.length + 1 || i === 1) typing = !typing;
    if (i === 1) typingMistake = !typingMistake;

    let timeout = rnd(50, 150);
    if (!typing) timeout = 50;
    if (i === currentText.length + 1) {
      timeout = typingMistake ? 0 : 5000;
    }
    timer = setTimeout(tick, timeout);
  };
  timer = setTimeout(tick, Math.random() * 1000);
  return {
    destroy() {
      clearTimeout(timer);
    },
  };
}
